FROM java:8-jdk-alpine
COPY ./target/demo-crm-1.0.jar /usr/app/
WORKDIR /usr/app
RUN sh -c 'touch demo-crm-1.0.jar'
ENTRYPOINT ["java","-jar","demo-crm-1.0.jar"]