package com.crm.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

import com.crm.demo.model.Cliente;
import com.crm.demo.model.Resultado;
import com.crm.demo.service.ClienteService;

@RestController
@RequestMapping("api")
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class ClientesController {
	
	@Autowired
	private ClienteService clienteService;
	
	/**
	 * Servicio para crear un cliente
	 * @param cliente
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@PostMapping(path = "/guardarCliente", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Resultado> guardarCliente(@RequestBody Cliente cliente) {
		Resultado resultadoDto = new Resultado();
		System.out.println(cliente.getNumeroDocumento());
		boolean resultado = clienteService.guardarCliente(cliente);
		resultadoDto.setError(!resultado);
		
		if(resultado) {
			resultadoDto.setMensaje("Cliente guardado!");
			return new ResponseEntity<>(resultadoDto, HttpStatus.OK);
		}else {
			resultadoDto.setMensaje("Ocurrio un error inesperado!");
			return new ResponseEntity<>(resultadoDto, HttpStatus.BAD_REQUEST);
		}
		
	}
	
	/**
	 * Servicio para consultar un cliente especifico tipo y num doc
	 * @param cliente
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@PostMapping("/consultarCliente")
	public Cliente consultarCliente(@RequestBody Cliente cliente) {
		System.out.println(cliente.getNumeroDocumento());
		return clienteService.buscarCliente(cliente);
	}
	
	/**
	 * Servicio para consultar todos los clientes
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@GetMapping("/consultarClientes")
	public List<Cliente> consultarClientes() {
		return clienteService.buscarClientes();
	}
	
	/**
	 * Servicio para consultar los tipo documentos guardados
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@GetMapping("/consultarTipoDoc")
	public List<String> consultarDocumentos() {
		return clienteService.consultarDocumentos();
	}

}
