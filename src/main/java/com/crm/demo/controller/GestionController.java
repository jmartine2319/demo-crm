package com.crm.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import com.crm.demo.model.Cliente;
import com.crm.demo.model.GestionDto;
import com.crm.demo.model.Resultado;
import com.crm.demo.service.GestionService;

@RestController
@RequestMapping("api")
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class GestionController {
	@Autowired
	private GestionService gestionService;
	
	/**
	 * Servicio para crear una gestion relacionada a un cliente
	 * @param gestion
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@PostMapping(path = "/crearGestion", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Resultado> crearGestion(@RequestBody GestionDto gestion) {
		Resultado resultadoDto = new Resultado();
		boolean resultado = gestionService.guardarGestion(gestion);
		resultadoDto.setError(!resultado);
		
		if(resultado) {
			resultadoDto.setMensaje("gestion guardada!");
			return new ResponseEntity<>(resultadoDto, HttpStatus.OK);
		}else {
			resultadoDto.setMensaje("Ocurrio un error inesperado!");
			return new ResponseEntity<>(resultadoDto, HttpStatus.BAD_REQUEST);
		}
		
	} 
	
	/**
	 * Se consultar todas las gestiones realizadas a un cliente
	 * @param cliente
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@PostMapping(path = "/obtenerGestiones", consumes = "application/json", produces = "application/json")
	public ResponseEntity<List<GestionDto>> obtenerGestion(@RequestBody Cliente cliente) {
		List<GestionDto> listaGestiones = gestionService.consultarGestiones(cliente);
		return new ResponseEntity<>(listaGestiones, HttpStatus.OK);
		
	} 

}
