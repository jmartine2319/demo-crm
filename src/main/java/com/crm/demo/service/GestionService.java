package com.crm.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crm.demo.entities.ClientesEntitie;
import com.crm.demo.entities.GestionEntitie;
import com.crm.demo.model.Cliente;
import com.crm.demo.model.GestionDto;
import com.crm.demo.repository.ClienteRepository;
import com.crm.demo.repository.GestionRepository;

@Component
public class GestionService {
	@Autowired
	private GestionRepository gestionRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	
	public boolean guardarGestion(GestionDto gestion) {
		ClientesEntitie clienteEnt = clienteRepository.findByTipoDocumentoAndNumeroDocumento(gestion.getTipoDocumento(), gestion.getNumeroDocumento());
		if(clienteEnt==null) {
			return false;
		}
		GestionEntitie gestionEnt = new GestionEntitie();
		gestionEnt.setObservacion(gestion.getComentario());
		gestionEnt.setEstado(gestion.getEstado());
		gestionEnt.setCliente(clienteEnt);
		GestionEntitie gestionOld = gestionRepository.save(gestionEnt);
		if(gestionOld==null) {
			return false;
		}else {
			return true;
		}
		
	}
	
	public List<GestionDto> consultarGestiones(Cliente cliente) {
		List<GestionDto> listaGestion = new ArrayList<>();
		ClientesEntitie clienteEnt = clienteRepository.findByTipoDocumentoAndNumeroDocumento(cliente.getTipoDocumento(), cliente.getNumeroDocumento());
		if(clienteEnt==null) {
			return listaGestion;
		}
		List<GestionEntitie> listaGestionEnt = gestionRepository.findByCliente(clienteEnt);
		for(GestionEntitie gestion:listaGestionEnt) {
			GestionDto gestionDto = new GestionDto();
			gestionDto.setComentario(gestion.getObservacion());
			gestionDto.setEstado(gestion.getObservacion());
			gestionDto.setNumeroDocumento(gestion.getCliente().getNumeroDocumento());
			gestionDto.setTipoDocumento(gestion.getCliente().getTipoDocumento());
			listaGestion.add(gestionDto);
		}
		return listaGestion;
		
	}
}
