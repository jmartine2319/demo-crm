package com.crm.demo.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crm.demo.entities.ClientesEntitie;
import com.crm.demo.model.Cliente;
import com.crm.demo.repository.ClienteRepository;

@Component
public class ClienteService {
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Transactional
	public boolean guardarCliente(Cliente cliente) {
		ClientesEntitie clienteEntitie = new ClientesEntitie();
		clienteEntitie.setApellido(cliente.getApellido());
		clienteEntitie.setNombre(cliente.getNombre());
		clienteEntitie.setCorreo(cliente.getCorreo());
		clienteEntitie.setNumeroDocumento(cliente.getNumeroDocumento());
		clienteEntitie.setTipoDocumento(cliente.getTipoDocumento());
		clienteEntitie.setCelular(cliente.getCelular());
		ClientesEntitie clienteEntitieOld = clienteRepository.save(clienteEntitie);
		if(clienteEntitieOld!=null) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public Cliente buscarCliente(Cliente cliente) {
		ClientesEntitie clienteEnt = clienteRepository.findByTipoDocumentoAndNumeroDocumento(cliente.getTipoDocumento(), cliente.getNumeroDocumento());
		Cliente clienteDto= new Cliente();
		if(clienteEnt==null) {
			return clienteDto;
		}
		clienteDto.setApellido(clienteEnt.getApellido());
		clienteDto.setNombre(clienteEnt.getNombre());
		clienteDto.setTipoDocumento(clienteEnt.getTipoDocumento());
		clienteDto.setNumeroDocumento(clienteEnt.getNumeroDocumento());
		clienteDto.setCelular(clienteEnt.getCelular());
		clienteDto.setCorreo(clienteEnt.getCorreo());
		return clienteDto;
	}
	
	public List<Cliente> buscarClientes() {
		List<ClientesEntitie> clientesEnt = clienteRepository.findAll();
		List<Cliente> clientesDto= new ArrayList<Cliente>();
		if(clientesEnt==null) {
			return clientesDto;
		}
		for(ClientesEntitie clienteEnt:clientesEnt) {
			Cliente clienteDto = new Cliente();
			clienteDto.setApellido(clienteEnt.getApellido());
			clienteDto.setNombre(clienteEnt.getNombre());
			clienteDto.setTipoDocumento(clienteEnt.getTipoDocumento());
			clienteDto.setNumeroDocumento(clienteEnt.getNumeroDocumento());
			clienteDto.setCelular(clienteEnt.getCelular());
			clienteDto.setCorreo(clienteEnt.getCorreo());
			clientesDto.add(clienteDto);
		}
		
		return clientesDto;
	}
	
	public List<String> consultarDocumentos() {
		List<String> listoTipoDoc = clienteRepository.findDocumentos();
		return listoTipoDoc;
	}
}
