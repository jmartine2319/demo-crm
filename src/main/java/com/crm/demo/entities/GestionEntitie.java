package com.crm.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="GESTION_CRM")
@Data
public class GestionEntitie {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="sec_gestion")
    @SequenceGenerator(name = "sec_gestion", sequenceName = "sec_gestion", allocationSize = 100)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="FK_NUM_DOCUMENTO_CLI", referencedColumnName = "NUM_DOCUMENTO_CLI", nullable = false)
    private ClientesEntitie cliente;
    
    @Column(name="ESTADO_GES", nullable=false, unique=false)
    private String estado;
    
    @Column(name="OBSERVACION_GES", nullable=false, unique=false)
    private String observacion;
}
