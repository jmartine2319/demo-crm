package com.crm.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="CLIENTES_CRM")
@Data
public class ClientesEntitie {

	@Column(name="TIPO_DOCUMENTO_CLI", nullable=false, unique=false)
    private String tipoDocumento;
	
	@Id
	@Column(name="NUM_DOCUMENTO_CLI", nullable=false, unique=true)
	private Integer numeroDocumento;
    
    @Column(name="NOMBRE_CLI", nullable=false, unique=false)
    private String nombre;
    
    @Column(name="APELLIDO_CLI", nullable=false, unique=false)
    private String apellido;
    
    @Column(name="CORREO_CLI", nullable=false, unique=false)
    private String correo;
    
    @Column(name="CELULAR_CLI", nullable=false, unique=false)
    private Long celular;
}
