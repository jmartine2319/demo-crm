package com.crm.demo.model;

import lombok.Data;

@Data
public class GestionDto {
	private String tipoDocumento;
	private Integer numeroDocumento;
	private String comentario;
	private String estado;
}
