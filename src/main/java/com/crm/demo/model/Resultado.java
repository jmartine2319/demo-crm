package com.crm.demo.model;

import lombok.Data;

@Data
public class Resultado {
	private boolean error;
	private String mensaje;
}
