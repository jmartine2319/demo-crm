package com.crm.demo.model;

import lombok.Data;

@Data
public class Cliente {
	private String tipoDocumento;
	private Integer numeroDocumento;
	private String nombre;
	private String apellido;
	private String correo;
	private Long celular;
}
