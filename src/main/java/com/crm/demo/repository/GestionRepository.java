package com.crm.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crm.demo.entities.ClientesEntitie;
import com.crm.demo.entities.GestionEntitie;

public interface GestionRepository extends JpaRepository<GestionEntitie, Long>{

	List<GestionEntitie> findByCliente(ClientesEntitie cliente);
}
