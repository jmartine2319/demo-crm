package com.crm.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.crm.demo.entities.ClientesEntitie;

public interface ClienteRepository extends JpaRepository<ClientesEntitie, Integer>{
	ClientesEntitie findByTipoDocumentoAndNumeroDocumento(String tipoDocumento,Integer numeroDocumento);
	@Query("SELECT distinct(a.tipoDocumento) FROM ClientesEntitie a")
	List<String> findDocumentos();

}
